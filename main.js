const tabs = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tabs-content li');


tabs.forEach((tab, index) => {
  tab.addEventListener('click', () => {
    tabs.forEach(tab => tab.classList.remove('active'));
    tab.classList.add('active');
    tabContent.forEach(content => content.style.display = 'none');
    tabContent[index].style.display = 'block';
  });
});

